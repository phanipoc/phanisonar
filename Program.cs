﻿using System;

namespace phanisonar
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = 1;
            Console.WriteLine("Hello World!");
        }
    }

    class A
    {
        private int x;
        private int y;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        public int Y
        {
            get { return x; }  // Noncompliant: field 'y' is not used in the return value
            set { x = value; } // Noncompliant: field 'y' is not updated
        }
    }
}
